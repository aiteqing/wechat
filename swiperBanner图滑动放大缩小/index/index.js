Page({
  data: {
    indicatorDots: false,
    autoplay: false,
    interval: 5000,
    duration: 1000,
    swiperIndex:0,
    imgUrls: [
      'http://yun-dev.5jiaju.cn/20190122/favm4jzjr4j.jpg',
      'http://yun-dev.5jiaju.cn/20190122/ylv99mmqm87.png',
      'http://yun-dev.5jiaju.cn/20190122/5fbu0uzo5ye.png'
    ]
  },
  swiperChange(e) {
    const that = this;
    that.setData({
      swiperIndex: e.detail.current,
    })
  }
})